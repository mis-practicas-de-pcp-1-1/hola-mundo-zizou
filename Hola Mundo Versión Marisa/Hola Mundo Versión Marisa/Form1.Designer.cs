﻿namespace Hola_Mundo_Versión_Marisa
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnHola = new System.Windows.Forms.Button();
            this.btnAdiós = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pbxAdiós = new System.Windows.Forms.PictureBox();
            this.pbxHola = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxAdiós)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHola)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Goldenrod;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 135F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnHola, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnAdiós, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(370, 224);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::Hola_Mundo_Versión_Marisa.Properties.Resources.CbWa22vXEAAL8NC;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(129, 124);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.DoubleClick += new System.EventHandler(this.pictureBox1_DoubleClick);
            // 
            // btnHola
            // 
            this.btnHola.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnHola.Location = new System.Drawing.Point(3, 133);
            this.btnHola.Name = "btnHola";
            this.btnHola.Size = new System.Drawing.Size(129, 41);
            this.btnHola.TabIndex = 1;
            this.btnHola.Text = "Hola";
            this.btnHola.UseVisualStyleBackColor = true;
            this.btnHola.Click += new System.EventHandler(this.btnHola_Click);
            // 
            // btnAdiós
            // 
            this.btnAdiós.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAdiós.Location = new System.Drawing.Point(3, 180);
            this.btnAdiós.Name = "btnAdiós";
            this.btnAdiós.Size = new System.Drawing.Size(129, 41);
            this.btnAdiós.TabIndex = 1;
            this.btnAdiós.Text = "Adiós";
            this.btnAdiós.UseVisualStyleBackColor = true;
            this.btnAdiós.Click += new System.EventHandler(this.btnAdiós_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.pbxAdiós);
            this.panel1.Controls.Add(this.pbxHola);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(138, 3);
            this.panel1.Name = "panel1";
            this.tableLayoutPanel1.SetRowSpan(this.panel1, 3);
            this.panel1.Size = new System.Drawing.Size(229, 218);
            this.panel1.TabIndex = 2;
            // 
            // pbxAdiós
            // 
            this.pbxAdiós.Image = global::Hola_Mundo_Versión_Marisa.Properties.Resources.Kirisame_Marisa_full_1612479;
            this.pbxAdiós.Location = new System.Drawing.Point(94, 45);
            this.pbxAdiós.Name = "pbxAdiós";
            this.pbxAdiós.Size = new System.Drawing.Size(100, 164);
            this.pbxAdiós.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxAdiós.TabIndex = 0;
            this.pbxAdiós.TabStop = false;
            this.pbxAdiós.Visible = false;
            // 
            // pbxHola
            // 
            this.pbxHola.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(207)))), ((int)(((byte)(101)))));
            this.pbxHola.Image = global::Hola_Mundo_Versión_Marisa.Properties.Resources.Kirisame_Marisa_full_1612478;
            this.pbxHola.Location = new System.Drawing.Point(16, 9);
            this.pbxHola.Name = "pbxHola";
            this.pbxHola.Size = new System.Drawing.Size(100, 170);
            this.pbxHola.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxHola.TabIndex = 0;
            this.pbxHola.TabStop = false;
            this.pbxHola.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(370, 224);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Kirisame Marisa";
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxAdiós)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHola)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnHola;
        private System.Windows.Forms.Button btnAdiós;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pbxAdiós;
        private System.Windows.Forms.PictureBox pbxHola;
    }
}

