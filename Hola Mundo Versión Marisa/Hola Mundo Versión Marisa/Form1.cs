﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hola_Mundo_Versión_Marisa
{
    
    public partial class Form1 : Form
    {
        /*
            Formulario de Hola Mundo Personalizado que además del saludo de hola
            permite dar el saludo de despedida (adiós).
            De acuerdo a sí se cliquea el botón de Hola o Adiós, veríamos a Marisa
            saludando feliz o despidiendose triste.
        */
        public Form1()
        {
            InitializeComponent();
        }

        private void btnHola_Click(object sender, EventArgs e)
        {
            // Imagen de hola llena todo el espacio del panel contenedor
            pbxHola.Dock = DockStyle.Fill;

            pbxHola.Visible = true;   // Hacemos visible la imagen de Hola
            pbxAdiós.Visible = false; // Hacemos invisible la imagen de Adiós

            // Modificando el título del formulario
            this.Text = "Kirisame marisa: ¡Hola Mundo! ^^";
        }

        private void btnAdiós_Click(object sender, EventArgs e)
        {
            // Imagen de hola llena todo el espacio del panel contenedor
            pbxAdiós.Dock = DockStyle.Fill;

            pbxHola.Visible = false;
            pbxAdiós.Visible = true;
            
            // Modificando el título del formulario
            this.Text = "Kirisame marisa: Adiós Mundo...";
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            pbxHola.Visible = false;  // Hacemos invisible la imagen de Hola
            pbxAdiós.Visible = false; // Hacemos invisible la imagen de Adiós

            // Modificando el título del formulario
            this.Text = "Kirisame marisa";
        }
    }
}
